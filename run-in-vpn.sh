#!/bin/bash

# set -euxo pipefail

cd ~/work/repos/pixelbook-fedora-setup/files/

 ~/vpn.sh proton-connect
 
echo Waiting for connection to be good...
false
while [ $? -ne 0 ];
do
  sleep 1s
  host google.co.uk 2>&1 >/dev/null
done

set -euxo pipefail 

sudo snap refresh
sudo flatpak update
sudo dnf update --refresh
ansible-playbook app-installs.yml -i hosts.yaml -e "login_user=$USER" -e "zerotier_network=e5cd7a9e1c66f72f"

 ~/vpn.sh proton-disconnect
