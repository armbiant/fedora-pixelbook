# Setup Fedora on Pixelbook

A basic ansible playbook for running the commands from https://github.com/jmontleon/pixelbook-fedora

Optionally setup passwordless sudo to make life a bit easier and make you enter your password a few less times (Google it if you don't know how to do this)

*WARNING*: Before running this script, and for your own protection, make sure you have a backup of your files and/or a disk image of your Pixelbook via something like CloneZilla in case something goes wrong while running this setup.

```
sudo dnf update

git clone https://gitlab.com/blenderfox/pixelbook-fedora-setup.git

cd pixelbook-fedora-setup/files

./run-ansible.sh
```

The `run-app-installs.sh` script runs a playbook for installing the apps I personally use -- you don't need to run it unless you want to have the same applications I do. :-)

The `run-app-installs-office.sh` script runs the app installs, but only installs a subset of the apps -- namely ones that are directly related to office work, and no non-office-related stuff, so no Steam, no Spotify, no Wine, etc.
