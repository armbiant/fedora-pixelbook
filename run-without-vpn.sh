#!/bin/bash

# set -euxo pipefail

cd ~/work/repos/pixelbook-fedora-setup/files/

sudo snap refresh
sudo flatpak update
sudo dnf update --refresh
ansible-playbook app-installs.yml -i hosts.yaml -e "login_user=$USER" -e "zerotier_network=e5cd7a9e1c66f72f"

