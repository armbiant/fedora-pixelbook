FROM registry.fedoraproject.org/fedora:34

RUN dnf install -y ansible dnf-plugins-core curl unzip git gpg && dnf update -y
COPY files /root/
WORKDIR /root/
